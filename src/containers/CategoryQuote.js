import React, { Component } from 'react';
import axios from '../axios-info';
import Quotes from '../components/UI/Quotes/Quotes';
import Spinner from '../components/UI/Spinner/Spinner';

class CategoryQuote extends Component {
  state = {
    quotes: [],
    category: '',
    id: [],
  }
  setInfoInState = () =>{
    // this.setState({loading : true});
    axios.get('' + this.props.location.pathname + '.json')
    .then((response)=> {
      const newQuotes = [];
      const id = [];
      for (let obj in response.data) {
        id.push(Object.keys(response.data))
        console.log(response);
          newQuotes.push(response.data[obj])
      };
      this.setState({quotes: newQuotes, id})
    })
    // .finally(this.setState({loading : false}));
  };
  editQuote = (index, category) => {
      this.props.history.push('edit/' + this.state.id[index]);
  };
  remuveQuote = (index, category) =>{
    axios.delete(this.props.location.pathname + '/' + this.state.id[index] +'.json').then(console.log(this.state.id[index])).then(()=> this.setInfoInState());
    };
  componentWillMount(){
    this.setInfoInState();
  };
  render() {
    // if (this.state.loading) {
    // return <Spinner />;
  // }
    return (
      <div className="App">
        <Quotes
          remuve={this.remuveQuote}
          edit={this.editQuote}
          quotes={this.state.quotes}
        />
      </div>
    );
  }
}

export default CategoryQuote;
