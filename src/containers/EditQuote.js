import React, { Component } from 'react';
import Forms from '../components/UI/Forms/Forms';
import axios from '../axios-info';


class EditQuoite extends Component {
  state = {
    author: '',
    category: '',
    text: '',
  };

  setInfoInState = () =>{
    // this.setState({loading : true});
    axios.get('' + this.props.location.pathname + '.json')
    .then((response)=> {
      console.log(response);
      const newQuotes = [];
      for (let obj in response.data) {
        console.log('sss' + response.data);
          newQuotes.push(response.data[obj])
      };
      this.setState({quotes: newQuotes})
    })
  }

  getAuthor = (event) => {
    this.setState({author: event.target.value});
  };
  getCatrgory = (event) => {
      this.setState({category: event.target.value});
  };
  getText = (event) => {
    this.setState({text: event.target.value});
  };
  addNewPostInServer = (e) => {
    e.preventDefault();
    const newQuote = {
      author: this.state.author,
      category: this.state.category,
      text: this.state.text,
      };
    axios.put('/all/' + this.state.category + '.json', newQuote);
  };
  componentDidMount(){
    this.setInfoInState();
    console.log(this.props.location.pathname);
  };
  render(){
    return (
        <Forms
          author={this.state.author}
          text={this.state.text}
          changeAuthor={this.getAuthor}
          slectCategory={this.getCatrgory}
          changeText={this.getText}
          clicked={this.addNewPostInServer}
        />
    )
  }
};

export default EditQuoite;
