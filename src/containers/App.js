import React, { Component, Fragment } from 'react';
import {Switch, Route} from 'react-router-dom';
import AddQuoite from './AddQuote';
import EditQuoite from './EditQuote';
import AllQuotes from './AllQuotes';
import CategoryQuote from './CategoryQuote';
import Header from '../components/Header/Header';


class App extends Component {
  render() {
    return (
      <Fragment>
        <Header />
        <Route path='/add' component={AddQuoite} />
        <Route path='/all/edit' component={EditQuoite} />
        <Route exact path='/' component={AllQuotes} />
        <Route exact path='/all' component={AllQuotes} />
        <Route exact path='/all/starwars' component={CategoryQuote} />
        <Route exact path='/all/saying' component={CategoryQuote} />
        <Route exact path='/all/homour' component={CategoryQuote} />
        <Route exact path='/all/famouspeople' component={CategoryQuote} />
        <Route exact path='/all/motivational' component={CategoryQuote} />
      </Fragment>
    );
  }
};



export default App;
