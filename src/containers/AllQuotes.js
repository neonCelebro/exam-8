import React, { Component } from 'react';
import axios from '../axios-info';
import Quotes from '../components/UI/Quotes/Quotes';
import Spinner from '../components/UI/Spinner/Spinner';

class AllQuotes extends Component {
  state = {
    quotes: [],
    id: [],
  }
  setInfoInState = () =>{
    // this.setState({loading : true});
    axios.get('all/.json')
    .then((response)=> {
      console.log(response);
      const newQuotes = [];
      const id = [];
      for (let obj in response.data) {
        for (let key in response.data[obj]){
          id.push(Object.keys(response.data[obj]));
          newQuotes.push(response.data[obj][key])
        };
      };
      this.setState({quotes: newQuotes, id})
    })
    // .finally(this.setState({loading : false}));
  };
  editQuote = (index, category) => {
      this.props.history.push('all/edit/' + this.state.id[index]);
  };
  remuveQuote = (index, category) =>{
    axios.delete(this.props.location.pathname + '/' + category  + '/' + this.state.id[index] +'.json').then(console.log(this.props.location.pathname + '/' + category  + '/' + this.state.id[index])).then(()=> this.setInfoInState());
  };
  componentDidMount(){
    this.setInfoInState();
    console.log(this.props.location.pathname);
  };

  render() {
    // if (this.state.loading) {
    // return <Spinner />;
  // }
    return (
      <div className="App">
        <Quotes
          quotes={this.state.quotes}
          remuve={this.remuveQuote}
          edit={this.editQuote}
        />
      </div>
    );
  }
}

export default AllQuotes;
