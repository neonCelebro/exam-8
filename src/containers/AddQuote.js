import React, { Component } from 'react';
import Forms from '../components/UI/Forms/Forms';
import axios from '../axios-info';


class AddQuoite extends Component {
  state = {
    author: '',
    category: '',
    text: '',
  };

  getAuthor = (event) => {
    this.setState({author: event.target.value});
  };
  getCatrgory = (event) => {
      this.setState({category: event.target.value});
  };
  getText = (event) => {
    this.setState({text: event.target.value});
  };
  addNewPostInServer = (e) => {
    e.preventDefault();
    const newQuote = {
      author: this.state.author,
      category: this.state.category,
      text: this.state.text,
      };
    axios.post('/all/' + this.state.category + '.json', newQuote)
    .then(this.setState({author: '', category: '', text: ''}));
  };
  render(){
    return (
        <Forms
          author={this.state.author}
          text={this.state.text}
          changeAuthor={this.getAuthor}
          slectCategory={this.getCatrgory}
          changeText={this.getText}
          clicked={this.addNewPostInServer}
        />
    )
  }
};

export default AddQuoite;
