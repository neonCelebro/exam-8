import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://quotes-exam.firebaseio.com/' // Your URL here!
});

export default instance;
