import React from 'react';
import './Quotes.css';

const Quotes = props => {
  console.log(props.quotes);
  return(
    <div className='quoteContainer'>
      {props.quotes.map((quote, id)=>{
        return(
          <div className='quotes' key={id}>
            <p>{quote.text}</p>
            <p>- {quote.author}</p>
            <button onClick={()=> props.edit(id, quote.category)}>Edit</button>
            <button onClick={()=> props.remuve(id, quote.category)}>Remuve</button>
          </div>
        )
      })}
    </div>
  )
};

export default Quotes;
