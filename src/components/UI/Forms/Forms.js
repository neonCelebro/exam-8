import React from 'react';
import './Forms.css';

const Forms = props => {
  return (
    <form className='boxForm' >
      <select required onChange={props.slectCategory}>
        <option></option>
        <option value="starwars">StarWars</option>
        <option value="famouspeople">Fampus people</option>
        <option value="saying">Saying</option>
        <option value="homour">Homour</option>
        <option value="motivational">Motivational</option>
      </select>
      <span className='cost'>
        <input
          className='inputs'
          value={props.author}
          onChange={props.changeAuthor}
          maxLength='50'
          required
          placeholder='Автор'
          type='text'
        />
      </span>
      <textarea required onChange={props.changeText} value={props.text} cols='100' rows='10' ></textarea>
      <input onClick={props.clicked} className='inputs' type='submit' id='sendNewData' value ='Отправить'/>
    </form>
  )
};

export default Forms;
