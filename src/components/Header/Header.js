import React, {Component, Fragment} from 'react';
import { NavLink } from 'react-router-dom';
import './Header.css';

class Header extends Component {
  render(){
    return(
      <Fragment>
        <div className='controls'>
          <NavLink className='btn control'  to="/add">Submit new Quote</NavLink>
          <NavLink  className='btn control' exact to="/">Quotes</NavLink>
        </div>
        <div className='categories'>
          <NavLink  className='btn' exact to="/all">all</NavLink>
          <NavLink  className='btn' exact to="/all/starwars">star Wars</NavLink>
          <NavLink  className='btn' to="/all/famouspeople">Famous people</NavLink>
          <NavLink  className='btn' to="/all/saying">Saying</NavLink>
          <NavLink  className='btn' to="/all/homour">Homour</NavLink>
          <NavLink  className='btn' to="/all/motivational">Motivational</NavLink>
        </div>
      </Fragment>
    )
  }
};

export default Header;
